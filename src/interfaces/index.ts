import * as actionTypes from '../actions/actionTypes';

export interface IRootState {
  data: Array<any>;
}

export interface ItemObject {
  indexX: number;
  neighbours: number;
  status: number;
}

export interface SystemState {
  data: Array<Array<ItemObject>>;
}

export interface SetNextStatus {
  type: typeof actionTypes.SET_NEXT_STATUS;
  payload: {
    data: Array<any>;
  };
}
