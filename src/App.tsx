import React, { useEffect, useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { nextTick } from './actions';
import { IRootState, ItemObject } from './interfaces';
import { findNeighbours, recalculateStatus } from './Utils';
import Loader from './components/Loader';
import Button from './components/Button';

import './styles/app.scss';

const App: React.FC = () => {
  const dispatch = useDispatch();
  const data = useSelector((state: IRootState) => state.data);

  const handleNextTick = useCallback(() => {
    const nextStatusData = recalculateStatus(data);
    const withNeighbours = findNeighbours(nextStatusData);

    dispatch(nextTick(withNeighbours));
  }, []);

  const renderFields = () =>
    data ? (
      data.map((row: Array<ItemObject>, index: number) => (
        <div key={index + 'row'} className="row">
          {renderCells(row)}
        </div>
      ))
    ) : (
      <Loader />
    );

  const renderCells = (row: Array<ItemObject>) =>
    row.map((el: ItemObject, index: number) => (
      <div className={`${!!el.status} cell`} key={index.toString() + 'cell'}>
        {el.neighbours}
      </div>
    ));

  return (
    <div className={`container`}>
      <Button value="Next tick" handler={handleNextTick} />
      {renderFields()}
    </div>
  );
};

export default App;
