import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { configureStore } from './store/configureStore';
import App from './App';
var store = configureStore();
ReactDOM.render(
  React.createElement(Provider, { store: store }, React.createElement(App, null)),
  document.getElementById('root')
);
