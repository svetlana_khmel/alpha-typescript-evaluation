import recalculateStatus from './recalculateStatus';
var fieldLength = 50;
var isAlive = function(obj) {
  return obj.status === 1;
};
export var checkRight = function(el, field, indX, indY) {
  return el[indX + 1] && isAlive(field[indY][indX + 1]);
};
export var checkLeft = function(el, field, indX, indY) {
  return el[indX - 1] && isAlive(field[indY][indX - 1]);
};
export var checkTop = function(el, field, indX, indY) {
  return field[indY - 1] && isAlive(field[indY - 1][indX]);
};
export var checkTopRight = function(field, indX, indY) {
  return field[indY - 1][indX + 1] && isAlive(field[indY - 1][indX + 1]);
};
export var checkTopLeft = function(field, indX, indY) {
  return field[indY - 1][indX - 1] && isAlive(field[indY - 1][indX - 1]);
};
export var checkBottom = function(el, field, indX, indY) {
  return el[indY + 1] && el[indY + 1] && isAlive(field[indY + 1][indX]);
};
export var checkBottomRight = function(field, indX, indY) {
  return field[indY + 1][indX + 1] && isAlive(field[indY + 1][indX + 1]);
};
export var checkBottomLeft = function(field, indX, indY) {
  return field[indY + 1][indX - 1] && isAlive(field[indY + 1][indX - 1]);
};
var findNeighbours = function(field) {
  field.forEach(function(el, indY, subArray) {
    subArray.forEach(function(elInSub, indX) {
      var currentObj = field[indY][indX];
      if (checkRight(el, field, indX, indY)) {
        currentObj.neighbours += 1;
      }
      if (checkLeft(el, field, indX, indY)) {
        currentObj.neighbours += 1;
      }
      if (checkTop(field, indX, indY)) {
        currentObj.neighbours += 1;
        if (checkTopRight(field, indX, indY)) {
          currentObj.neighbours += 1;
        }
        if (checkTopLeft(field, indX, indY)) {
          currentObj.neighbours += 1;
        }
      }
      if (checkBottom(el, field, indX, indY)) {
        currentObj.neighbours += 1;
        if (checkBottomRight(field, indX, indY)) {
          currentObj.neighbours += 1;
        }
        if (checkBottomLeft(field, indX, indY)) {
          currentObj.neighbours += 1;
        }
      }
    });
  });
  return field;
};
var createRandomData = function() {
  var field = [];
  for (var y = 0; y < fieldLength; y++) {
    field[y] = new Array(fieldLength).fill(null).map(function(el, i) {
      return { indexX: i, neighbours: 0, status: Math.round(Math.random()) };
    });
  }
  return findNeighbours(field);
};
export { createRandomData, findNeighbours, recalculateStatus };
