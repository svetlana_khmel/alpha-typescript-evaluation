import { field } from '../../__test-data__';
import {
  findNeighbours,
  checkRight,
  checkLeft,
  checkTop,
  checkTopRight,
  checkTopLeft,
  checkBottom,
  checkBottomRight,
  checkBottomRight,
  checkBottomLeft
} from '../';

describe('Found right neighbours', () => {
  it('Should find right neighbour ', () => {
    const data = field;
    const el = [
      { indexX: 1, neighbours: 0, status: 0 },
      { indexX: 2, neighbours: 0, status: 1 },
      { indexX: 3, neighbours: 0, status: 1 }
    ];
    const indX = 0;
    const indY = 0;
    const expectedOutcome = true;

    expect(checkRight(el, data, indX, indY)).toEqual(expectedOutcome);
  });

  it('Should find left neighbour ', () => {
    const data = field;
    const el = [
      { indexX: 1, neighbours: 0, status: 0 },
      { indexX: 2, neighbours: 0, status: 1 },
      { indexX: 3, neighbours: 0, status: 1 }
    ];
    const indX = 2;
    const indY = 0;
    const expectedOutcome = true;

    expect(checkLeft(el, data, indX, indY)).toEqual(expectedOutcome);
  });

  it('Should find top neighbour ', () => {
    const data = [
      [
        { indexX: 1, neighbours: 0, status: 0 },
        { indexX: 2, neighbours: 0, status: 1 },
        { indexX: 3, neighbours: 0, status: 0 }
      ],
      [
        { indexX: 4, neighbours: 0, status: 0 },
        { indexX: 5, neighbours: 0, status: 1 },
        { indexX: 6, neighbours: 0, status: 0 }
      ]
    ];

    const indX = 1;
    const indY = 1;
    const expectedOutcome = true;

    expect(checkTop(data, indX, indY)).toEqual(expectedOutcome);
  });

  it('Should find TopRight neighbour ', () => {
    const data = [
      [
        { indexX: 1, neighbours: 0, status: 0 },
        { indexX: 2, neighbours: 0, status: 1 },
        { indexX: 3, neighbours: 0, status: 1 }
      ],
      [
        { indexX: 4, neighbours: 0, status: 0 },
        { indexX: 5, neighbours: 0, status: 1 },
        { indexX: 6, neighbours: 0, status: 0 }
      ]
    ];

    const indX = 1;
    const indY = 1;
    const expectedOutcome = true;

    expect(checkTopRight(data, indX, indY)).toEqual(expectedOutcome);
  });

  it('Should find TopLeft neighbour ', () => {
    const data = [
      [
        { indexX: 1, neighbours: 0, status: 1 },
        { indexX: 2, neighbours: 0, status: 1 },
        { indexX: 3, neighbours: 0, status: 0 }
      ],
      [
        { indexX: 4, neighbours: 0, status: 0 },
        { indexX: 5, neighbours: 0, status: 1 },
        { indexX: 6, neighbours: 0, status: 0 }
      ]
    ];

    const indX = 2;
    const indY = 1;
    const expectedOutcome = true;

    expect(checkTopLeft(data, indX, indY)).toEqual(expectedOutcome);
  });

  it('Should find bottom neighbour ', () => {
    const data = [
      [
        { indexX: 1, neighbours: 0, status: 0 },
        { indexX: 2, neighbours: 0, status: 1 },
        { indexX: 3, neighbours: 0, status: 0 }
      ],
      [
        { indexX: 4, neighbours: 0, status: 0 },
        { indexX: 5, neighbours: 0, status: 1 },
        { indexX: 6, neighbours: 0, status: 0 }
      ]
    ];

    const indX = 1;
    const indY = 0;
    const expectedOutcome = true;

    expect(checkBottom(data, indX, indY)).toEqual(expectedOutcome);
  });

  it('Should find BottomRight neighbour ', () => {
    const data = [
      [
        { indexX: 1, neighbours: 0, status: 0 },
        { indexX: 2, neighbours: 0, status: 1 },
        { indexX: 3, neighbours: 0, status: 0 }
      ],
      [
        { indexX: 4, neighbours: 0, status: 0 },
        { indexX: 5, neighbours: 0, status: 0 },
        { indexX: 6, neighbours: 0, status: 1 }
      ]
    ];

    const indX = 1;
    const indY = 0;
    const expectedOutcome = true;

    expect(checkBottomRight(data, indX, indY)).toEqual(expectedOutcome);
  });

  it('Should find BottomLeft neighbour ', () => {
    const data = [
      [
        { indexX: 1, neighbours: 0, status: 0 },
        { indexX: 2, neighbours: 0, status: 1 },
        { indexX: 3, neighbours: 0, status: 0 }
      ],
      [
        { indexX: 4, neighbours: 0, status: 1 },
        { indexX: 5, neighbours: 0, status: 0 },
        { indexX: 6, neighbours: 0, status: 0 }
      ]
    ];

    const indX = 1;
    const indY = 0;
    const expectedOutcome = true;

    expect(checkBottomLeft(data, indX, indY)).toEqual(expectedOutcome);
  });
});
