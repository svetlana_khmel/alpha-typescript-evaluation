import recalculateStatus from '../recalculateStatus';

describe('Recalculated new statuses', () => {
  it('If alive and 2 neighbours should stay alive (status 1) ', () => {
    const data = [[{ indexX: 1, neighbours: 2, status: 1 }]];
    const expectedOutcome = [[{ indexX: 1, neighbours: 0, status: 1 }]];

    expect(recalculateStatus(data)).toEqual(expectedOutcome);
  });

  it('If alive and 3 neighbours should stay alive (status 1) ', () => {
    const data = [[{ indexX: 1, neighbours: 3, status: 1 }]];
    const expectedOutcome = [[{ indexX: 1, neighbours: 0, status: 1 }]];

    expect(recalculateStatus(data)).toEqual(expectedOutcome);
  });

  it('If alive and less then 2 neighbours should not stay alive (status 0) ', () => {
    const data = [[{ indexX: 1, neighbours: 1, status: 1 }]];
    const expectedOutcome = [[{ indexX: 1, neighbours: 0, status: 0 }]];

    expect(recalculateStatus(data)).toEqual(expectedOutcome);
  });
  it('If alive and more then 3 neighbours should not stay alive (status 0) ', () => {
    const data = [[{ indexX: 1, neighbours: 4, status: 1 }]];
    const expectedOutcome = [[{ indexX: 1, neighbours: 0, status: 0 }]];

    expect(recalculateStatus(data)).toEqual(expectedOutcome);
  });

  it('If not alive and  3 neighbours should be alive (status 1) ', () => {
    const data = [[{ indexX: 1, neighbours: 3, status: 0 }]];
    const expectedOutcome = [[{ indexX: 1, neighbours: 0, status: 1 }]];

    expect(recalculateStatus(data)).toEqual(expectedOutcome);
  });
});
