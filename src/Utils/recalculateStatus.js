var recalculateStatus = function(data) {
  return data.map(function(subArray) {
    return subArray.map(function(el) {
      if (el.status === 1) {
        if (el.neighbours === 2 || el.neighbours === 3) {
          el.status = 1;
        }
        if (el.neighbours < 2 || el.neighbours > 3) {
          el.status = 0;
        }
      }
      if (el.status === 0 && el.neighbours === 3) {
        el.status = 1;
      }
      el.neighbours = 0;
      return el;
    });
  });
};
export default recalculateStatus;
