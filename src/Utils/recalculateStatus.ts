import { ItemObject } from '../interfaces';

const recalculateStatus = (data: Array<Array<ItemObject>>) =>
  data.map((subArray: Array<ItemObject>) => {
    return subArray.map((el: ItemObject) => {
      if (el.status === 1) {
        if (el.neighbours === 2 || el.neighbours === 3) {
          el.status = 1;
        }
        if (el.neighbours < 2 || el.neighbours > 3) {
          el.status = 0;
        }
      }

      if (el.status === 0 && el.neighbours === 3) {
        el.status = 1;
      }
      el.neighbours = 0;
      return el;
    });
  });

export default recalculateStatus;
