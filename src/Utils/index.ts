import { ItemObject } from '../interfaces/';
import recalculateStatus from './recalculateStatus';

const fieldLength = 50;

const isAlive = (obj: ItemObject): boolean => obj.status === 1;

export const checkRight = (
  el: Array<ItemObject>,
  field: Array<Array<ItemObject>>,
  indX: number,
  indY: number
): boolean => el[indX + 1] && isAlive(field[indY][indX + 1]);

export const checkLeft = (
  el: Array<ItemObject>,
  field: Array<Array<ItemObject>>,
  indX: number,
  indY: number
): boolean => el[indX - 1] && isAlive(field[indY][indX - 1]);

export const checkTop = (field: Array<Array<ItemObject>>, indX: number, indY: number): boolean =>
  field[indY - 1] && isAlive(field[indY - 1][indX]);

export const checkTopRight = (field: Array<Array<ItemObject>>, indX: number, indY: number): boolean =>
  field[indY - 1][indX + 1] && isAlive(field[indY - 1][indX + 1]);

export const checkTopLeft = (field: Array<Array<ItemObject>>, indX: number, indY: number): boolean =>
  field[indY - 1][indX - 1] && isAlive(field[indY - 1][indX - 1]);

export const checkBottom = (field: Array<Array<ItemObject>>, indX: number, indY: number): boolean =>
  field[indY + 1] && isAlive(field[indY + 1][indX]);

export const checkBottomRight = (field: Array<Array<ItemObject>>, indX: number, indY: number): boolean =>
  field[indY + 1][indX + 1] && isAlive(field[indY + 1][indX + 1]);

export const checkBottomLeft = (field: Array<Array<ItemObject>>, indX: number, indY: number): boolean =>
  field[indY + 1][indX - 1] && isAlive(field[indY + 1][indX - 1]);

const findNeighbours = (field: Array<Array<ItemObject>>): Array<Array<ItemObject>> => {
  for (let indY = 0; indY < fieldLength; indY++) {
    for (let indX = 0; indX < fieldLength; indX++) {
      const currentObj = field[indY][indX];
      const el = field[indY];

      if (checkRight(el, field, indX, indY)) {
        currentObj.neighbours += 1;
      }

      if (checkLeft(el, field, indX, indY)) {
        currentObj.neighbours += 1;
      }

      if (checkTop(field, indX, indY)) {
        currentObj.neighbours += 1;

        if (checkTopRight(field, indX, indY)) {
          currentObj.neighbours += 1;
        }

        if (checkTopLeft(field, indX, indY)) {
          currentObj.neighbours += 1;
        }
      }

      if (checkBottom(field, indX, indY)) {
        currentObj.neighbours += 1;

        if (checkBottomRight(field, indX, indY)) {
          currentObj.neighbours += 1;
        }

        if (checkBottomLeft(field, indX, indY)) {
          currentObj.neighbours += 1;
        }
      }
    }
  }

  return field;
};

const createRandomData = (): Array<Array<ItemObject>> => {
  let field: any[] = [];

  for (let y = 0; y < fieldLength; y++) {
    field[y] = new Array(fieldLength)
      .fill(null)
      .map((el, i) => ({ indexX: i, neighbours: 0, status: Math.round(Math.random()) }));
  }

  return findNeighbours(field);
};

export { createRandomData, findNeighbours, recalculateStatus };
