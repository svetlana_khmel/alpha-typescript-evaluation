import { createStore } from 'redux';
import rootReducer from '../reducers';
export var configureStore = function() {
  return createStore(rootReducer);
};
