import { makeMockStore } from './utils';

const mockField = [
  [
    {
      indexX: 0,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 1,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 2,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 3,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 4,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 5,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 6,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 7,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 8,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 9,
      neighbours: 0,
      status: 1
    }
  ],
  [
    {
      indexX: 0,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 1,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 2,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 3,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 4,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 5,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 6,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 7,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 8,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 9,
      neighbours: 0,
      status: 0
    }
  ],
  [
    {
      indexX: 0,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 1,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 2,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 3,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 4,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 5,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 6,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 7,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 8,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 9,
      neighbours: 0,
      status: 1
    }
  ],
  [
    {
      indexX: 0,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 1,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 2,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 3,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 4,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 5,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 6,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 7,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 8,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 9,
      neighbours: 0,
      status: 0
    }
  ],
  [
    {
      indexX: 0,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 1,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 2,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 3,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 4,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 5,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 6,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 7,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 8,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 9,
      neighbours: 0,
      status: 1
    }
  ],
  [
    {
      indexX: 0,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 1,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 2,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 3,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 4,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 5,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 6,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 7,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 8,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 9,
      neighbours: 0,
      status: 1
    }
  ],
  [
    {
      indexX: 0,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 1,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 2,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 3,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 4,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 5,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 6,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 7,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 8,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 9,
      neighbours: 0,
      status: 1
    }
  ],
  [
    {
      indexX: 0,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 1,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 2,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 3,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 4,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 5,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 6,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 7,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 8,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 9,
      neighbours: 0,
      status: 0
    }
  ],
  [
    {
      indexX: 0,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 1,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 2,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 3,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 4,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 5,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 6,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 7,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 8,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 9,
      neighbours: 0,
      status: 1
    }
  ],
  [
    {
      indexX: 0,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 1,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 2,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 3,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 4,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 5,
      neighbours: 0,
      status: 0
    },
    {
      indexX: 6,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 7,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 8,
      neighbours: 0,
      status: 1
    },
    {
      indexX: 9,
      neighbours: 0,
      status: 1
    }
  ]
];

const field = [
  [
    { indexX: 1, neighbours: 0, status: 0 },
    { indexX: 2, neighbours: 0, status: 1 },
    { indexX: 3, neighbours: 0, status: 1 }
  ],
  [
    { indexX: 4, neighbours: 0, status: 0 },
    { indexX: 5, neighbours: 0, status: 0 },
    { indexX: 6, neighbours: 0, status: 0 }
  ],
  [
    { indexX: 7, neighbours: 0, status: 1 },
    { indexX: 8, neighbours: 0, status: 0 },
    { indexX: 9, neighbours: 0, status: 1 }
  ]
];

export { makeMockStore, mockField, field };
