var __assign =
  (this && this.__assign) ||
  function() {
    __assign =
      Object.assign ||
      function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
var mockStore = configureStore([thunk]);
export var makeMockStore = function(state) {
  if (state === void 0) {
    state = {};
  }
  return mockStore(__assign({}, state));
};
