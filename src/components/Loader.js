import React, { memo } from 'react';
import '../styles/loader.scss';
var Loader = function() {
  return React.createElement('div', { className: 'lds-dual-ring' });
};
export default memo(Loader);
