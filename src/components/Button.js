import React, { memo } from 'react';
var Button = function(_a) {
  var value = _a.value,
    handler = _a.handler;
  return React.createElement('button', { onClick: handler }, value);
};
export default memo(Button);
