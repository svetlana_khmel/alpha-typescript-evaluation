import React, { memo } from 'react';

type ButtonProps = {
  value: string;
  handler?: IFunction;
};

interface IFunction {
  (): void;
}

const Button: React.FC<ButtonProps> = ({ value, handler }) => <button onClick={handler}>{value}</button>;

export default memo(Button);
