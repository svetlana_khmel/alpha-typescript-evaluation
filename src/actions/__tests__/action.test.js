import { nextTick } from '../';
import * as actions from '../actionTypes';
import { mockField } from '../../__test-data__/utils';

describe('Actions', () => {
  it('Should create an action to next tick ', () => {
    const data = mockField;
    const expectedAction = {
      type: actions.SET_NEXT_STATUS,
      payload: data
    };

    expect(nextTick(data)).toEqual(expectedAction);
  });
});
