import * as actionTypes from './actionTypes';
var nextTick = function(data) {
  return { type: actionTypes.SET_NEXT_STATUS, payload: data };
};
export { nextTick };
