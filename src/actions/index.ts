import * as actionTypes from './actionTypes';

import { SetNextStatus } from '../interfaces';

const nextTick = (data: any): SetNextStatus => ({ type: actionTypes.SET_NEXT_STATUS, payload: data });

export { nextTick };
