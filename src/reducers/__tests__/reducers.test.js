import reducer from '../';
import * as actions from '../../actions/actionTypes';
import { mockField } from '../../__test-data__';

describe('test reducers', () => {
  it('should handle SET_NEXT_STATUS', () => {
    const payload = mockField;
    const result = { data: mockField };

    expect(
      reducer(
        {},
        {
          type: actions.SET_NEXT_STATUS,
          payload
        }
      )
    ).toEqual(result);
  });
});
