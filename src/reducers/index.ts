import * as actionTypes from '../actions/actionTypes';
import { SystemState } from '../interfaces';
import { createRandomData } from '../Utils';

const initialState: SystemState = {
  data: createRandomData()
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_NEXT_STATUS:
      return { ...state, data: action.payload };

    default:
      return state;
  }
};

export default reducer;
