var __assign =
  (this && this.__assign) ||
  function() {
    __assign =
      Object.assign ||
      function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
import * as actionTypes from '../actions/actionTypes';
import { createRandomData } from '../Utils';
var initialState = {
  data: createRandomData()
};
var reducer = function(state, action) {
  if (state === void 0) {
    state = initialState;
  }
  switch (action.type) {
    case actionTypes.SET_NEXT_STATUS:
      return __assign(__assign({}, state), { data: action.payload });
    default:
      return state;
  }
};
export default reducer;
