import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { nextTick } from './actions';
import { findNeighbours, recalculateStatus } from './Utils';
import Loader from './components/Loader';
import Button from './components/Button';
import './styles/app.scss';
var App = function() {
  var dispatch = useDispatch();
  var data = useSelector(function(state) {
    return state.data;
  });
  var handleNextTick = useCallback(function() {
    var nextStatusData = recalculateStatus(data);
    var withNeighbours = findNeighbours(nextStatusData);
    dispatch(nextTick(withNeighbours));
  }, []);
  var renderFields = function() {
    return data
      ? data.map(function(row, index) {
          return React.createElement('div', { key: index + 'row', className: 'row' }, renderCells(row));
        })
      : React.createElement(Loader, null);
  };
  var renderCells = function(row) {
    return row.map(function(el, index) {
      return React.createElement(
        'div',
        { className: !!el.status + ' cell', key: index.toString() + 'cell' },
        el.neighbours
      );
    });
  };
  return React.createElement(
    'div',
    { className: 'container' },
    React.createElement(Button, { value: 'Next tick', handler: handleNextTick }),
    renderFields()
  );
};
export default App;
